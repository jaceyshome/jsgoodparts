
var myApp = {
	weapons:[
		{
			name:"knife",
			maxBullets:NaN,
			bullets:NaN,
			speed:20,
			damage:50,
			distance:2
		},
		{
			name:"pistol",
			maxBullets:12,
			bullets:12,
			speed:10,
			damage:2,
			distance:10
		},
		{
			name:"shotgun",
			maxBullets:8,
			bullets:8,
			speed:5,
			damage:10,
			distance:5
		},
		{
			name:"sniper",
			maxBullets:5,
			bullets:5,
			speed:2,
			damage:100,
			distance:100
		}
	],
	players:[],
	enemies:[],
	game:[],
	createPlayer:function(initData){
		this.players.push(new Player(initData))
	},
	addEnemy:function(initData){
		this.enemies.push(new Enemy(initData))
	},
	removeEnemy:function(){},
	removePlayer:function(){}
};



var GameObj = function(customData){
	var defaultData = {
		name:"gameObj",
		hp:100,
		maxHp:100,
		weapon:undefined,
		color:"white",
		special:"",
		isDead:false
	};
	$.extend(this, defaultData );
};

GameObj.prototype = {
	damage : function(value){
		this.hp -= value;
		return this;
	},
	checkHp : function(){
		if(this.hp <= 0){
			this.hp = 0;
			this.isDead = true;
		}
		return this;
	},
	reset : function(){
		this.isDead = false;
		this.hp = 0;
		return this;
	}
};

var Player = function(customData){
	GameObj.call(this);
	var defaultData = {
		name:"defaultPlayer",
		experience:0,
		nextExperience:100,
		level:0
	};

	$.extend(this, defaultData, customData);
	//private variables
	var _killingNumber = 10;
	var _points = 1100;
	//private functions
	var upgrade = function(){console.log("upgrade level");}
};

var ctor = function(){};
ctor.prototype = GameObj.prototype;
Player.prototype = new ctor();

Player.prototype.heal = function(item){
	this.hp += item.value;
	return this;
};

Player.prototype.reset = function(){
	GameObj.prototype.reset.apply(this, arguments);
	this.experience = -10;
	return this;
};

var player1 = new Player({name:"playerA"});
var player2 = new Player({name:"playerB"});
myApp.players.push(player1);
myApp.players.push(player2);
player1.reset();
console.log(player1);
var gameObj1 = new GameObj();
console.log(gameObj1);

var Enemy = function(customData){
	GameObj.call(this);
	var defaultData = {
		name:"defaultEnemy",
		speed: 10
	};
	$.extend(this, defaultData, customData);
};

Enemy.prototype = Object.create(GameObj.prototype);

Enemy.prototype.animate = function(){
	console.log("animate enemy");
};

var enemy1 = new Enemy();
console.log("enemy1:", enemy1);

var healAllPlayers = function(item){
	var _item = item || {name:"healAll", value:10};
	myApp.players.map(
		function(player){
			player.hp += _item.value;
			return player;
		});
};

healAllPlayers();
console.log(myApp.players);
