/**
 * Created by developer5 on 11/02/14.
 */


//document.writeln('hello world'.toUpperCase());

//100 = 1e2

//falsy values:
/*
	false
	null
	undefined
	The empty string ''
	The number 0
	The number NaN
*/

//set default value ||
var width = NaN;
var newWidth = width || 1;


document.writeln(newWidth);


//prototype
console.log("============= prototype ================");

//Define a functional object to hold persons in JavaScript
var Person = function(name) {
	this.name = name;
};

//Add dynamically to the already defined object a new getter
Person.prototype.getName = function() {
	return this.name;
};

//Create a new object of type Person
var john = new Person("John");

//Try the getter
document.writeln(john.getName());

//If now I modify person, also John gets the updates
Person.prototype.sayMyName = function() {
	document.writeln('Hello, my name is ' + this.getName());
};

//Call the new method on john
john.sayMyName();
//Until now I've been extending the base object, now I create another object and then inheriting from Person.

//Create a new object of type Customer by defining its constructor. It's not
//related to Person for now.
var Customer = function(name) {
	this.name = name;
};

//Now I link the objects and to do so, we link the prototype of Customer to
//a new instance of Person. The prototype is the base that will be used to
//construct all new instances and also, will modify dynamically all already
//constructed objects because in JavaScript objects retain a pointer to the
//prototype
Customer.prototype = new Person();

//Now I can call the methods of Person on the Customer, let's try, first
//I need to create a Customer.
var myCustomer = new Customer('Dream Inc.');
myCustomer.sayMyName();

//If I add new methods to Person, they will be added to Customer, but if I
//add new methods to Customer they won't be added to Person. Example:
Customer.prototype.setAmountDue = function(amountDue) {
	this.amountDue = amountDue;
};
Customer.prototype.getAmountDue = function() {
	return this.amountDue;
};

//Let's try:
myCustomer.setAmountDue(2000);
document.writeln(myCustomer.getAmountDue());
//While as said I can't call setAmountDue(), getAmountDue() on a Person.

//The following statement generates an error.
//john.setAmountDue(1000);


var Company = function(name, location,age){
	this.name = name;
	this.location = location;
	this.age = age;
};

Company.prototype.getName = function(){
	return this.name;
};

Company.prototype.getLocation = function(){
	return this.location;
};

Company.prototype.getAge = function(){
	return this.age;
};

var nike = new Company("nike","USA","50");
console.log(nike.getAge());
console.log(nike.getLocation());
console.log(nike.getName());
console.log(nike);
console.log("-------------------------");
var obj;
for(obj in nike){
	console.log("obj", nike[obj]);
}

//invocation
console.log("=============invocation================");

function hello(thing) {
	console.log(this + " says hello " + thing);
}


hello.call("Yehuda", "world"); //=> Yehuda says hello world

console.log("============= nested function of 'this' meaning ================");

var o = { // An object o.
	m: function() { // Method m of the object.
		var self = this; // Save the this value in a variable.
		console.log("M",this); // Prints "true": this is the object o.
		 // Now call the helper function f().
		var f = function () { // A nested function f
			console.log(this); // "false": this is global or undefined
			console.log(self); // "true": self is the outer this value.
		};
		f();
		return this;
	},
	n:function(){
		console.log("N this", this);
		var nA = {
			nAf: function(){
				console.log(this);
			}
		};
		nA.nAf();
	}
};
o.m().n(); // Invoke the method m on the object o.
//o.n(); // Invoke the method m on the object o.

var reset = function(){
	var play = function(){
		console.log("play!!!");
	};
	return {
		play:play
	}
};

reset().play();

console.log("================== method chain example ===============");

var shape = {
	x:0,
	y:0,
	z:0,
	scaleX:1,
	scaleY:1,
	setX:function(_x){this.x = _x; return this;},
	setY:function(_y){this.y = _y; return this;},
	setZ:function(_z){this.z = _z; return this;},
	draw:function(){
		console.log("shapeX:",this.x);
		console.log("shapeY:",this.y);
		console.log("shapeZ:",this.z);
	}
};

shape.setX(10).setY(10).setZ(10).draw();

console.log("============== the deference of function and method ====");

var sayHello = function(){
	console.log("hello world");
};
sayHello();
var tellSayHello = {
	sayHello: sayHello
};

tellSayHello.sayHello();

sayHello = function(){
	console.log("Goodbye world");
};
sayHello();
tellSayHello.sayHello();

console.log("===================== lexical scope =================");

var scopeA = "global scope"; // A global variable
function checkscopeA() {
	var scopeA = "local scope"; // A local variable
	function fA() {
		return scopeA;
	} // Return the value in scope here
	return fA();
}
console.log("scopeA return:",checkscopeA()); // => "local scope"

var scopeB = "global scope"; // A global variable
function checkscopeB() {
	var scopeB = "local scope"; // A local variable
	function fB() {
		return scopeB;
	} // Return the value in scope here
	return fB;
}
console.log("scopeB return:",checkscopeB()()); // What does this return?

console.log("===================== array functions example ============ ");
// First, define two simple functions
var sum = function(x,y) { return x+y; };
var square = function(x) { return x*x; };
// Then use those functions with Array methods to compute mean and stddev
var data = [1,1,3,5,5];
var mean = data.reduce(sum)/data.length;
console.log("after reduce", mean);
var deviations = data.map(function(x) {return x-mean;});
var stddev = Math.sqrt(deviations.map(square).reduce(sum)/(data.length-1));
console.log("stddev",stddev);

console.log("=================== scope ==================");
var foo = function ( ) {
	var a = 3, b = 5;
	var bar = function ( ) {
		var b = 7, c = 11;
// At this point, a is 3, b is 7, and c is 11
		a += b + c;
		console.log("a:", a);
// At this point, a is 21, b is 7, and c is 11
	};
// At this point, a is 3, b is 5, and c is not defined
	bar( );
// At this point, a is 21, b is 5
};

foo();

console.log("================= closure ===================");

var myObject = function ( ) {
	var value = 0;
	return {
		increment: function (inc) {
			value += typeof inc === 'number' ? inc : 1;
		},
		getValue: function ( ) {
			return value;
		}
	};
}( );

console.log("myObject", myObject);

console.log("=================== Wrapper objects ==============");
var s = "test", n = 1, b = true; // A string, number, and boolean value.
var S = new String(s); // A String object
var N = new Number(n); // A Number object
var B = new Boolean(b); // A Boolean object

console.log("S",S);
console.log("N",N);
console.log("B",B);

console.log("===================== scope chain =================");

// a globally-scoped variable
var a = 1;

// global scope
function one(){
	console.log("this global",a);
}

one();

// local scope
function two(a){
	console.log("local value",a);
}

two(2);

// local scope again
function three1(){
	var a = 3;
	console.log("local value again",a);
}

three1();

// local scope again
function three2(){
	console.log("three2",a); // alerts "undefined", cur local "a" is defined in local scope already,but not defined when alert()
	var a = 3;
	console.log("three2 again",a);
}

three2();

// Intermediate: no such thing as block scope in javascript

function four1(){
	if(true){
		var a=4;
	}
	console.log("alerts '4'", a); // alerts '4', not the global value of '1'
}

four1();

function four2(){
	console.log("alerts 'undefined'", a); // alerts 'undefined',same reason as that of three2();
	if(true){
		var a=4;
	}
}

four2();

// Intermediate: object properties
function Five(){
	this.a = 5;
}

console.log("new five", new Five().a);



// Advanced: closure
var six = function(){
	var foo = 6;
	return function(){
		// javascript "closure" means I have access to foo in here,
		// because it is defined in the function in which I was defined.
		console.log("foo", foo);
	}
}();

six();


// Advanced: prototype-based scope resolution
function Seven(){
	this.a = 7;
}

// [object].prototype.property loses to [object].property in the scope chain
Seven.prototype.a = -1; // won't get reached, because 'a' is set in the constructor above.
Seven.prototype.b = 8; // Will get reached, even though 'b' is NOT set in the constructor.

console.log("new seven a", new Seven().a);
console.log("new seven b", new Seven().b);


function Night(){
	this.a = 9;
	console.log("global night a",a);
}

Night();//if dont instantiate Night then this is actually current context----window.
console.log("global night a change",a);// So if this.a is changed in Night(),then the global a is changed as well.

console.log("================ four ways of declaration of functions ========");

//a function defined with the Function constructor assigned to the variable multiply
//should be avoid
console.log("multiplyA:",multiplyA);
var multiplyA = new Function("x", "y", "return x * y;");
console.log("multiplyA:",multiplyA);

//a function declaration of a function named multiply
console.log("multiplyB:",multiplyB);
function multiplyB(x, y) {
	return x * y;
}
console.log("multiplyB:",multiplyB);

//a function expression of an anonymous function assigned to the variable multiply
console.log("multiplyC:",multiplyC);
var multiplyC = function(x, y) {
	return x * y;
};
console.log("multiplyC:",multiplyC);

//function declarations and function expressions are faster than constructor

//a function expression of a function named func_name assigned to the variable multiply
console.log("multiplyD:",multiplyD);
var multiplyD = function func_name(x, y) {
	return x * y;
};
console.log("multiplyD:",multiplyD);

console.log(" ============ test function scope =====================");
var weapon = {
	bullets: [],
	speed: function speed(){
		console.log("speed of the weapon", this);
		function checkSelf(){
			console.log("check self again",this);
		}
		checkSelf();
	}

};
weapon.speed();

console.log("========== define functions conditionally, need to avoid , not work consistently cross-browser =====");
if (0) {
	function zero() {
		document.writeln("This is zero. this conditional function should be undefined");
	}
}
console.log(zero);

if (0){
	var zero1 = function(){
		document.writeln("This is zero. this conditional function should be undefined");
	}
}
console.log("it is correct if the value is undefined", zero1);


console.log("================== Closure =======================");
var myObject = function ( ) {
	var value = 0;
	return {
		increment: function (inc) {
			console.log("this", this);
			value += typeof inc === 'number' ? inc : 1;
		},
		getValue: function ( ) {
			console.log(value);
			return value;
		}
	};
}( );
console.log("myObject", myObject);
myObject.increment(1);
myObject.getValue();

var fade = function (node) {
	var level = 1;
	var step = function ( ) {
		var hex = level.toString(16);
		node.style.backgroundColor = '#FFFF' + hex + hex;
		if (level < 15) {
			level += 1;
			setTimeout(step, 100);
		}
	};
	setTimeout(step, 100);
};
fade(document.body);
console.log(fade);

console.log("==================== function array ==================");
testArys = ['ant', 'Bug', 'cat', 'Dog'];
testArys = testArys.concat(["bird"]);
console.log(testArys);
testNums = [1,2,3,4,5];
testNums.forEach(function(value, i, a){ a[i] =  value + 1});
console.log(testNums);

testObjs = [
	{name:"obj1", id:1},
	{name:"obj2", id:2},
	{name:"obj3", id:3},
	{name:"obj3", id:4},
	{name:"obj3", id:5}
];
testObjs.map(function(obj){ obj.id = obj.id + 100; return obj;});
console.log("TestObjs", testObjs);

console.log("after filter:", testObjs.filter(function(obj){
	if(obj.id > 102)
		return true;
	else
		return false;
	}));
console.log("testObjs every testing", testObjs.every(function(obj){
	if(obj.id > 104)
		return true;
	else
		return false;
}));
console.log("testObjs some testing", testObjs.some(function(obj){
	if(obj.id > 104)
		return true;
	else
		return false;
}));


console.log("============== testing number function: isNaN(), isFinite() =========");
console.log(isNaN(0));
console.log(isNaN("opps"));
console.log(isNaN(NaN));
console.log(isNaN("0"));

var isNumber = function isNumber(value) {
	return typeof value === 'number' && isFinite(value);
};

