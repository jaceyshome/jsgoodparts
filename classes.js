function inherit(p){
	var type;
	if (p === null){
		throw TypeError();
	}
	if (Object.create){
		return Object.create(p);
	}
	type = typeof p;
	if (t !== "object" && t !== "function") {
		throw TypeError();
	}
	function f(){};
	f.prototype = p;
	return new f();
}

//method 1 to create a new object, not a good way

function range(from, to){
	var r = inherit(range.methods);
	r.from = from;
	r.to = to;
	return r;
}

range.methods = {
	includes: function(x) { return this.from <= x && x <= this.to; },
	foreach: function(f) {
		for(var x = Math.ceil(this.from); x <= this.to; x++) f(x);
	},
	toString: function() { return "(" + this.from + "..." + this.to + ")"; }
};

var r = range(1,3);
console.log("method1 r:" , r);

r.includes(2);
r.foreach(function(value){
	console.log(value);
});


//method 2 to create a new object
function Range2(from, to){
	r.from = from;
	r.to = to;
}

Range2.prototype = {
	includes: function(x) { return this.from <= x && x <= this.to; },
	foreach: function(f) {
		for(var x = Math.ceil(this.from); x <= this.to; x++) f(x);
	},
	toString: function() { return "(" + this.from + "..." + this.to + ")"; }
};

var r2 = new Range2(1,3);
console.log("method2 r:" , r2);

r2.includes(2);
r2.foreach(function(value){
	console.log(value);
});

console.log( r instanceof Range2);
console.log( r2 instanceof Range2);

