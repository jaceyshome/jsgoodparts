console.log("=============== javascript function practice ==================");
if (!Function.prototype.bind) {
	Function.prototype.bind = function(o /*, args */) {
// Save the this and arguments values into variables so we can
// use them in the nested function below.
		var self = this, boundArgs = arguments;
// The return value of the bind() method is a function
		return function() {
// Build up an argument list, starting with any args passed
// to bind after the first one, and follow those with all args
// passed to this function.
			var args = [], i;
			for(i = 1; i < boundArgs.length; i++) args.push(boundArgs[i]);
			for(i = 0; i < arguments.length; i++) args.push(arguments[i]);
// Now invoke self as a method of o, with those arguments
			return self.apply(o, args);
		};
	};
}

var myApp = {
	weapons:[
		{
			name:"knife",
			maxBullets:NaN,
			bullets:NaN,
			speed:20,
			damage:50,
			distance:2
		},
		{
			name:"pistol",
			maxBullets:12,
			bullets:12,
			speed:10,
			damage:2,
			distance:10
		},
		{
			name:"shotgun",
			maxBullets:8,
			bullets:8,
			speed:5,
			damage:10,
			distance:5
		},
		{
			name:"sniper",
			maxBullets:5,
			bullets:5,
			speed:2,
			damage:100,
			distance:100
		}
	],
	players:[],
	enemies:[],
	game:[],
	createPlayer:function(initData){
		this.players.push(new Player(initData))
	},
	healingItems:[
		{name:"potion", value:10},
		{name:"superPotion", value:30},
		{name:"megaPotion", value:50}
	],
	heal : function(item){
//		console.log("healing with ", item.name, "+", item.value, "HP");
		this.hp += item.value;
//		console.log(this);
		return this;
	},
	damage: function(value){
		this.hp -= value;
	}

};

var Player = function(customData){
	var defaultData = {
		name:"player",
		hp:100,
		maxHp:100,
		weapon:undefined,
		color:"blue",
		experience:0,
		nextExperience:100,
		level:0,
		special:"",
		isDead:false,
		healingItems: [
			{ total:10 },
			{ total:30 },
			{ total:50 }
		]
	};
	$.extend(this, defaultData ,customData);
};

Player.prototype = {
	addExperience : function(exp){
		this.experience += exp;
		this.checkExperience();
	},

	checkExperience : function(){
		if(this.experience >= this.nextExperience){
			this.level++;
			this.nextExperience = this.nextExperience * 2;
		}
	},

	damage : function(value){
		return myApp.damage.bind(this)(value);
	},

	checkHp : function(){
		if(this.hp <= 0){
			this.hp = 0;
			this.isDead = true;
			console.log("player:", this.name, "is dead");
		}
	},

	reset : function(){
		this.isDead = false;
		this.experience = 0;
		this.level = 0;
		this.nextExperience = 100;
	},

	//second way
	heal: function(item){
		var self = this;
		if(item){
			return myApp.heal.bind(self)(item);
		}else{
			return {
				usePotion : function(){
					return myApp.heal.bind(self)(myApp.healingItems[0]);
				},
				useSuperPotion : function(){
					return myApp.heal.bind(self)(myApp.healingItems[1]);
				},
				useMegaPotion : function(){
					return myApp.heal.bind(self)(myApp.healingItems[2]);
				}
			};
		}
	}
};


var init = function(){
	initGameObjects();

	var playerA = {
		name:"Jake",
		hp: 150,
		maxHp:150,
		weapon:myApp.weapons[0]
	};

	myApp.createPlayer(playerA);
	myApp.players[0].damage(10);
//	myApp.players[0].healingItem.usePotion();
	myApp.players[0].heal().useSuperPotion();
	console.log("heal function", myApp.players[0].heal);
	console.log("name" in myApp.players[0]);
	console.log("name testing:",myApp.players[0].hasOwnProperty("name"));
	var healingPlayerAWithPotion = myApp.players[0].heal();
	console.log("healingPlayerAWithPotion",healingPlayerAWithPotion);
//	myApp.healingItems[1].hp = -5;
//	console.log(myApp.healingItems[1]);
	console.log(myApp.players[0]);
};

var initGameObjects = function(){

};

window.onload = init();


