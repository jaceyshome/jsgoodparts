var Component = function (name) {
	var component = Component._registry[name]();
	component.__uid = generateUID();
	return component;
};

Component._registry = {};

Component.add = function (name, factory) {
	Component._registry[name] = factory;
};

Component.add("transform", function() {
	return {
		translation: [0,0],
		rotation: 0,
		scale: 1
	}
});


Component.add("mover", function() {
	return {
		start: function(entity) {
			this._transform = entity.getComponent("transform");
		},
		update: function(deltaTime) {
			this._transform.translation[0]  = this._transform.translation[0] + 7 * deltaTime;
			this._transform.translation[1]  = this._transform.translation[1] + 7 * deltaTime;
			console.log(this._transform.translation);
		}
	};
});

Component.add("action", function() {
	return {
		shoot: function() {
			console.log("shoot player");
		}
	};
});

console.log(Component._registry);

var count = 0;
function generateUID () {
	var id = "Entity" + count;
	count += 1;
	return id;
}

var entities = [];

var Entity = function(name) {
	this.uid = generateUID();
	this.name = name;
	this._components = {};
	this.addComponent("transform");
	entities.push(this);
};

Entity.prototype = {
	addComponent: function (type) {
//		this._components[type] = Component(type).init(this); // check to see if init exists!
		return this._components[type] = Component(type);
	},
	getComponent: function (type) {
		return this._components[type];
	},
	removeComponent: function (type) {
		this._components.splice(type,1); //need to double check
	}
};

var gunner = new Entity("gunner");
gunner.addComponent("mover");
console.log(gunner);

var computerPlayerA = new Entity("player");
computerPlayerA.addComponent("action");
console.log(computerPlayerA);

console.log(entities);

var startedComponents = {};

function started (component) {
	return !!startedComponents[component.__uid];
}

function start (component, entity) {
	if (component.start) component.start(entity);
	startedComponents[component.__uid] = true;
}

function instantiatePrefab (data) {
	var key;
	var component;
	var componentData;
	var e = new Entity(data.name || "default");
	if (data.components) {
		for (key in data.components) {
			if (data.components.hasOwnProperty(key)) {
				componentData = data.components[key];
				component = e.addComponent(key);
				for (key in componentData) {
					if (component.hasOwnProperty(key)) {
						component[key] = componentData[key]; // probably want to do a deep copy instead of passing reference
					}
				}
			}
		}
	}
	// go through and call start on all components!
	return e;
}

e = instantiatePrefab({
	components: {
		transform: {
			translation: [10, 11],
			rotation: 90,
			scale: 10
		},
		mover: {}
	}
});

console.log(e);

/*setInterval(function() {
	//calculate delta time, here is temp. You want this value to be in seconds NOT milliseconds
	var delta = 1/60;
	var components;
	var component;
	var i;
	var key;
	for(i = 0; i < entities.length; i += 1) {
		components = entities[i]._components;
		for (key in components) {
			if (components.hasOwnProperty(key)) {
				component = components[key];
				if (!started(component)) {
					start(component, entities[i]);
				}
				if (component.update) component.update(delta);
			}
		}
	}
}, 1000/60);*/


